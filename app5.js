
/**
 * Module dependencies.
 * http://www.youtube.com/watch?feature=player_detailpage&v=qD-bvtKV1N8
 *
 */


var express = require('express'),
    mongoose = require('mongoose'),//http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=124s
 http = require('http')
;

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3001);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.errorHandler());
  
  
  app.locals.pretty = true; // http://stackoverflow.com/questions/7416455/turn-on-line-breaks-in-jade-source
  app.use(express.bodyParser());
  
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + "/public")); // http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=488s
  app.use(function(req, res){
    res.send(404, "four - oh - four");   
  });
  
  app.use(function(err, req, res, next){
      res.status(err.status || 404); // http://www.youtube.com/watch?feature=player_detailpage&v=YDwng3kzB2k#t=436s
      res.send(err.message);
  });
});

mongoose.connect("mongodb://root@4id.me/helloExpress"); // http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=213s

var UserSchema = new mongoose.Schema({
    name: String,
    email: String,
    age: Number
}),
    Users = mongoose.model('Users', UserSchema);

// http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=326s
app.get('/mongo/all', function(req, res) {
    Users.find({}, function (err, docs) {
        res.render('users/index', {users: docs});
    });
});

// new user: http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=428s
app.get('/mongo/new', function(req, res) {
    res.render('users/new');
});

// create user: http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=491s
app.post('/mongo/create', function(req, res) {
    var b = req.body;
    new Users({
        name: b.name,
        email: b.email,
        age: b.age
        }).save(function (err, user) {
            if(err) res.json(err);
            res.redirect('/mongo/'+user.name);
        });//http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=510s
});

// show mongo user http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=619s
app.param('name', function(req, res, next, name) {
    Users.find({name: name}, function(err, docs) {
        req.user = docs[0];
        next();//http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=650s
    });
})

app.get('/mongo/:name', function(req, res) {
    res.render("users/show", {user: req.user});//http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=663s
});

/*
    mongo Edit: http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=764s
*/
app.get('/mongo/:name/edit', function(req, res){
    res.render("users/edit", {user: req.user});
});

/*
    mongo Update
    http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=885s
*/
    app.put('/mongo/:name', function (req, res) {
        var b = req.body;
        Users.update(
            {name: req.params.name}, 
            {name: b.name, age: b.age, email: b.email},
            function(err){//http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=963s
                res.redirect("/mongo/"+b.name);
            });
    });


/*
    Delete:http://www.youtube.com/watch?feature=player_detailpage&v=RoSX87kZoJQ#t=1032s
*/
app.delete('/mongo/:name', function (req, res) {
    Users.remove({name:req.params.name}, function (err) {
        res.redirect('/mongo/all');
    });
});
app.get('/', function(req, res){
    res.render("home", { title: "Having fun with express. Ok! Good!"});
});

app.get('/ua', function(req, res) {
    res.send(req.get('user-agent')); // http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=63s
});

//http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=98s
app.get('/accepted', function(req, res) {
    res.send(req.accepted);
});

//http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=140s
app.get('/accepts', function(req, res) {
    res.send(req.accepts(['html', 'text', 'json']));
});

//http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=174s
app.get('/charsets', function(req, res) {
    res.send(req.acceptedCharsets);
});

//http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=198s
app.get('/utf', function(req, res) {
    res.send(req.acceptsCharset('utf-8') ? 'yes' : 'no');
});

//http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=268s
app.get('/lang', function(req, res) {
    res.send(req.acceptedLanguages);
});

//http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=287s
app.get('/langFr', function(req, res) {
    res.send(req.acceptsLanguage('fr') ? 'yes' : "no");
});


//http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=366s
app.get('/name/:name?', function(req, res) {
    res.send(req.params.name);
});

http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=389s
app.get('/nick/:name?', function(req, res) {
    res.send(req.param('name', 'default name'));
});

// Other requrest properties: http://www.youtube.com/watch?feature=player_detailpage&v=60WBsb_rLeQ#t=423s

// Response object:

//http://www.youtube.com/watch?feature=player_detailpage&v=S-DQ_YjCWWw#t=33s
app.get('/res', function(req, res) {
    res.send(404, 'not found');
});

//http://www.youtube.com/watch?feature=player_detailpage&v=S-DQ_YjCWWw#t=62s
app.get('/json', function(req, res) {
    res.json({message: "somethis"});
});

//http://www.youtube.com/watch?feature=player_detailpage&v=S-DQ_YjCWWw#t=145s
app.get('/image', function(req, res) {
    res.type('image/png').send('this is a picture');
});


//http://www.youtube.com/watch?feature=player_detailpage&v=S-DQ_YjCWWw#t=244s
// install curl: http://stackoverflow.com/questions/2939820/how-to-enable-curl-installed-ubuntu-lamp-stack
app.get("/format", function(req, res) {
    res.format({
        html: function () { res.send("<h1> Body </h1>");},
        json: function () { res.json({message: "body"});},
        text: function () {res.send("doby");}
        });
});

//http://www.youtube.com/watch?feature=player_detailpage&v=S-DQ_YjCWWw#t=339s
app.get("/redirect", function(req, res) {
    res.redirect("/");
});


//http://www.youtube.com/watch?feature=player_detailpage&v=S-DQ_YjCWWw#t=365s
app.get("/redirect301", function(req, res) {
    res.redirect(301, "/");
});

// other response methods: http://www.youtube.com/watch?feature=player_detailpage&v=S-DQ_YjCWWw#t=448s


/*

    Advanced Routes
    http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=28s

*/

var users = ['Иван', 'jeff', 'sally', 'sarah', 'kim', 'john', 'Женя'];




//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=83s
app.get('/users/:from-:to', function(req, res) {
    var from = parseInt(req.params.from, 10),
        to = parseInt(req.params.to, 10)
    
    res.json(users.slice(from, to + 1));
});

//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=326s
app.param('from', function (req, res, next) {
    req.from = parseInt(req.params.from, 10);
    next();
});

//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=338s
app.param('to', function(req, res, next, to) {
    req.to = parseInt(to, 10);
    next();
});


//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=389s
app.get('/users2/:from-:to', function(req, res) {
    res.json(users.slice(req.from, req.to + 1));
});


/*
    How many times static file was downloaded:
    http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=559s
*/
var count = 0;
app.get('/hello.txt', function(req, res) {
    res.send('overiding'); // http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=585s
});

//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=590s
app.get('/hello2.txt', function(req, res, next) {
    count++;
    next();
});

//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=622s
app.get("/count", function(req, res) {
    res.send("" + count +  " Views");
});

//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=816s
/*
    load user middleware
*/
var users3 = [{name: "Данила"}, {name: "Аделаида"}, {name: "Раиса"}];

function loadUser(req, res, next) {
    req.user = users3[parseInt(req.params.userId, 10)]
    next();
}

//http://www.youtube.com/watch?feature=player_detailpage&v=Xt-qJ8lNjiE#t=836s
app.get("/users3/:userId", loadUser, function(req, res) {
    res.json(req.user);
})

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

/*
    404 test
    http://www.youtube.com/watch?feature=player_detailpage&v=YDwng3kzB2k#t=280s
*/

app.get('/user404test/:username', function(req, res, next) {
    if(req.params.username === 'Аделаида')
    {
      next();
    } else{
        res.send(req.params.username + "'s profile");
    }
});

//http://www.youtube.com/watch?feature=player_detailpage&v=YDwng3kzB2k#t=363s
app.get('/user404error/:username', function(req, res, next) {
    if (req.params.username === 'Аделаида') {
        var err = new Error('User does not exist');
        next(err);
    }
    else {
        res.send(req.params.username + "'s profile");
    }
});

//http://www.youtube.com/watch?feature=player_detailpage&v=YDwng3kzB2k#t=547s
app.param('username', function(req, res, next, username){
    if(username !== 'Аделаида')
    {
        req.user = username;
        next();
    } 
    else
    {
        next(new Error("no user found"));
    }
});

app.get("/usersParam/:username", function(req, res, next){
    res.send(req.user + "'s profile");
});





