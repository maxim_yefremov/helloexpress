
/**
 * Module dependencies.
 */

var express = require('express'),
 http = require('http')
;

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3001);
  app.use(express.bodyParser());
});





app.get('/', function(req, res){
	res.send("Hello, Express!");
});

app.get("/hi", function(req, res){
    var message = ["<h1>Hello, Express</h1>",
			"<p>Welcome to 'Building Web Apps in Node.js with Express.'</p>",
			"<p>You'll love Express because it's</p>",
			"<ul><li>fast</li>",
				"<li>fun</li>",
				"<li>flexible</li></ul>"].join("\n");
                
    res.send(message);
});
/*
app.get("/user/:userId", function(req, res) {
    res.send("<h1>Hello, User #"+req.params.userId);
});
*/
app.post("/users", function (req, res) {
    res.send("Creating a new user with the name "+req.body.username + ".");
});

app.get(/\/users\/(\d*)\/?(edit)?/, function(req, res) {
    // /users/10
    // /users/10/
    // /users/10/edit
    
    var message = "user #" + req.params[0] + "'s profile";
    if(req.params[1] === 'edit')
    {
        message="Editing " + message;
    }
    else{
        message = "Viewing " + message;
    }
    res.send(message);
    
    console.log(req.params);
    
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
